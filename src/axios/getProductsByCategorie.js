import axios from "./axios-instance";

export const getProductsByCategorie = async categorieId => {
  const response = await axios.get(`/sites/MLA/search?category=${categorieId}`);
  return response.data;
};
