import axios from "./axios-instance";

export const getDescriptionByProduct = async (productID, descriptionID) => {
  const response = await axios.get(
    `/items/${productID}/descriptions/${descriptionID}`
  );
  return response.data;
};
