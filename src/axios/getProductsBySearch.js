import axios from './axios-instance';

export const getProductsBySearch = async query => {
  const response = await axios.get(`/sites/MLA/search?q=${query}`);
  return response.data;
};
