import axios from "./axios-instance";

export const getDescriptionByProduct = async categoryID => {
  const response = await axios.get(`/categories/${categoryID}`);
  const newData = {
    name: response.data.name,
    image: response.data.picture
  };
  return newData;
};
