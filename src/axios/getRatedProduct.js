import axios from "./axios-instance";

export const getRatedProduct = async productID => {
  const response = await axios.get(`/reviews/item/${productID}`);
  return response.data;
};
