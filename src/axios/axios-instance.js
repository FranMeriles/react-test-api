import axios from "axios";

const instance = axios.create({
  baseURL: "https://api.mercadolibre.com"
});

instance.interceptors.response.use( 
  response => response, 
  error => { 
    console.log(error, "Custom Error"); 
    throw error; 
  } 
);

export default instance;
