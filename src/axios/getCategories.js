import axios from "./axios-instance";

export const getCategories = async () => {
  const response = await axios.get("/sites/MLA/categories");
  return response.data;
};
