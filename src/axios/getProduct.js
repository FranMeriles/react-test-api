import axios from "./axios-instance";

export const getProduct = async productID => {
  const response = await axios.get(`/items/${productID}`);
  return response.data;
};
