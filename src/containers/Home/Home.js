import React, { useEffect, useState } from "react";
import Categories from "../../components/Home/Categories/Categories";
import ScrollToTop from "../../util/ScrollToTop";

import { getCategories } from "../../axios/getCategories";
import classes from "./Home.module.scss";

const Home = props => {
  const [categories, setCategories] = useState([]);

  useEffect(() => {
    getCategories().then(categories => setCategories(categories));
  }, []);

  return (
    <section className={classes.Home}>
      <ScrollToTop />
      <h2>Bienvenido</h2>
      <Categories categories={categories} />
    </section>
  );
};

export default Home;
