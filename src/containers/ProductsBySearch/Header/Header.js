import React from 'react';
import classes from './Header.module.scss';

const Header = props => {
  return (
    <section className={classes.Header}>
      <div className={classes.Content}>
        <h1>
          Resultados de busqueda de "<b>{props.q}</b>"
        </h1>
      </div>
    </section>
  );
};

export default Header;
