import React, { useState, useEffect } from 'react';

import { getProductsBySearch } from '../../axios/getProductsBySearch';

import Header from './Header/Header';
import Card from '../../UI/Card/Card';
import BottomHeader from "../../components/BottomHeader/BottomHeader"
import Spinner from "../../UI/Spinner/Spinner"

import useQuery from '../../util/getQueryParams';

import classes from './ProductsBySearch.module.scss';

const ProductsBySearch = props => {
  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(false);

  let query = useQuery().get('query');

  useEffect(() => {
    setLoading(true);
    getProductsBySearch(query).then(response => {
      setProducts(response.results);
      setLoading(false);
    });
  }, [query]);

  let contentProducts = <Spinner />;

  if (!loading) {
    contentProducts = products.map(prd => <Card product={prd} key={prd.id} />);
  }

  return (
    <>
      <BottomHeader />
      <Header q={query} />
      <section className={classes.ProductsBySearch}>
        { contentProducts }
      </section>
    </>
  );
};

export default ProductsBySearch;
