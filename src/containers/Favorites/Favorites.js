import React from "react";
import { useSelector } from "react-redux";

import BottomHeader from "../../components/BottomHeader/BottomHeader";
import Card from "../../UI/Card/Card"

import classes from "./Favorites.module.scss";

const Favorites = () => {

  const favorites = useSelector(state => state.favorites.products)

  let favoriteProducts = favorites.map(prd => <Card product={prd} key={prd.id} />);

  if (favorites.length === 0) {
    favoriteProducts = <h2>No hay productos en tus favoritos</h2>;
  }

  return (
    <>
      <BottomHeader />
      <section className={classes.FavoritesProducts}>
        { favoriteProducts }
      </section>    
    </>
  )
}

export default Favorites