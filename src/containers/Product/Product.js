import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux"
import * as favoritesActions from "../../store/actions/favorites"

import ImagesCarousel from "../../components/Product/ImagesCarousel/ImagesCarousel";
import ProductContent from "../../components/Product/ProductContent/ProductContent";
import ProductDescription from "../../components/Product/ProductDescription/ProductDescription";
import BottomHeader from "../../components/BottomHeader/BottomHeader"

import Heart from "../../assets/svg/Heart";

import ScrollToTop from "../../util/ScrollToTop";
import checkIfProductExistInFavorites from "../../util/checkIfProductExistInFavorites"

import { getProduct } from "../../axios/getProduct";
import { getDescriptionByProduct } from "../../axios/getDescriptionByProduct";
import { getRatedProduct } from "../../axios/getRatedProduct";

import classes from "./Product.module.scss";

const Product = props => {
  const [product, setProduct] = useState([]);
  const [description, setDescription] = useState([]);
  const [rated, setRated] = useState(0);
  const dispatch = useDispatch()
  const productID = props.match.params.id;
  const productsFavorites = useSelector(state => state.favorites.products)

  
  useEffect(() => {
    const getProductInfo = async () => {
      const product = await getProduct(productID);
      setProduct(product);
      const descriptionID = product.descriptions[0].id;
      const description = await getDescriptionByProduct(productID, descriptionID);
      setDescription(description.plain_text);
      const rated = await getRatedProduct(productID);
      setRated(rated)
    }

    getProductInfo()  

  }, [productID]);
  
  return (
    <>
      <ScrollToTop />     
      <section className={classes.Product}>
        <BottomHeader />
        <div className={classes.ProductContainer}>
          <div className={classes.LeftContent}>
            <div className={classes.Images}>
            <div className={classes.AddFavorite}>
            {
              checkIfProductExistInFavorites(productsFavorites, productID)         
              ?     <div onClick={() => dispatch(favoritesActions.removeFavorite(product.id))}><Heart type="full"/></div>
              :     <div onClick={() => dispatch(favoritesActions.addFavorite(product))}><Heart type="border"/></div>
            }        
            </div>
              <ImagesCarousel images={product.pictures} />
            </div>
            <div className={classes.Descriptions}>
              <ProductDescription
                attributes={product.attributes}
                descriptions={description}
              />
            </div>
          </div>
          <div className={classes.RightContent}>
            <ProductContent product={product} rate={rated.rating_average} />
          </div>
        </div>
      </section>
    </>
  );
};

export default Product;
