import React, { useState, useEffect } from "react";
import { getProductsByCategorie } from "../../axios/getProductsByCategorie";

import classes from "./ProductsByCategory.module.scss";

import Card from "../../UI/Card/Card";
import Header from "./Header/Header";
import Spinner from "../../UI/Spinner/Spinner";
import BottomHeader from "../../components/BottomHeader/BottomHeader"

import ScrollToTop from "../../util/ScrollToTop";

const ProductsByCategory = props => {
  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(false);

  const id = props.match.params.id;

  useEffect(() => {
    setLoading(true);
    getProductsByCategorie(id).then(response => {
      setProducts(response.results);
      setLoading(false);
    });
    return () => setLoading(false);
  }, [id]);

  let contentProducts = <Spinner />;

  if (!loading) {
    contentProducts = products.map(prd => <Card product={prd} key={prd.id} />);
  }
  
  return (
    <>
      <ScrollToTop />
      <BottomHeader />
      <Header id={id} />
      <section className={classes.ProductCategory}>{contentProducts}</section>
    </>
  );
};

export default ProductsByCategory;
