import React, { useState, useEffect } from "react";

import { getDescriptionByProduct } from "../../../axios/getCategoryDescription";
import classes from "./Header.module.scss";

const Header = ({ id }) => {
  const [category, setCategory] = useState({});

  useEffect(() => {
    getDescriptionByProduct(id).then(cat => setCategory(cat));
  }, [id]);
  return (
    <section className={classes.Header}>
      <div className={classes.Content}>
        <h1>{category.name}</h1>
        <img src={category.image} alt={category.name} />
      </div>
    </section>
  );
};

export default Header;
