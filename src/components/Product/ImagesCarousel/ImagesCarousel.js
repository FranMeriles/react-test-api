import React from "react";
import Slider from "react-slick";

import NextArrow from "../../../UI/slick/nextArrow";
import PrevArrow from "../../../UI/slick/prevArrow";

import Spinner from "../../../UI/Spinner/Spinner";

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

const ImagesCarousel = props => {
  const settings = {
    customPaging: i => {
      return (
        <img
          className="slick-thumbnail"
          src={props.images[i].url}
          alt={props.images[i].id}
        />
      );
    },
    dots: true,
    dotsClass: "slick-dots slick-thumb",
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    adaptiveHeight: true,
    nextArrow: <NextArrow />,
    prevArrow: <PrevArrow />
  };

  if (props.images) {
    return (
      <>
        
        <Slider {...settings}>
          {props.images.map(img => (
            <div style={{ textAlign: "center" }} key={img.id}>
              <img src={img.url} alt={img.id} />
            </div>
          ))}
        </Slider>
      </>
    );
  }
  return <Spinner />;
};

export default ImagesCarousel;
