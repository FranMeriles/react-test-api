import React from "react";
import classes from "./ProductDescription.module.scss";

const ProductDescription = props => {
  let attributes = null;

  if (props.attributes) {
    attributes = props.attributes.map(atr => {
      return (
        <div className={classes.Item} key={atr.id}>
          <span>{atr.name}</span>
          <p>{atr.value_name}</p>
        </div>
      );
    });
  }
  return (
    <>
      <h2 className={classes.SubTitle}>Descripción</h2>
      <p className={classes.Description}>{props.descriptions}</p>
      <br />
      <h2 className={classes.SubTitle}>Caracteristicas</h2>
      <div className={classes.Characteristics}>{attributes}</div>
    </>
  );
};

export default ProductDescription;
