import React from "react";

import ProductRated from "./ProductRated/ProductRated";

import { getPriceRound } from "../../../util/getPriceRound";
import formatPrice from "../../../util/formatPrice";

import Button from "../../../UI/Button/Button";
import Warranty from "../../../assets/svg/Warranty";

import classes from "./ProductContent.module.scss";

const ProductContent = props => {
  return (
    <aside className={classes.Content}>
      <p className={classes.TopText}>
        {props.product.condition === "new" ? "Nuevo" : "Usado"}
        <span> - {props.product.sold_quantity && props.product.sold_quantity.toLocaleString('de') } Vendidos</span>
      </p>
      <h1 className={classes.Title}>{props.product.title}</h1>
      {props.product.original_price && (
        <p className={classes.OldPrice}>
          {formatPrice(props.product.original_price)}
        </p>
      )}
      <ProductRated rate={props.rate} />
      <p className={classes.Price}>
        {formatPrice(props.product.price)}
        {props.product.original_price && (
          <span className={classes.PriceOff}>
            {getPriceRound(props.product.original_price, props.product.price)} % OFF
          </span>
        )}
      </p>
      <p>
        {props.product.available_quantity > 0
          ? "Stock disponible"
          : "Sin stock"}
      </p>
      <div className={classes.Actions}>
        <Button text="Comprar ahora" type="Large" color="Primary" />
        <Button text="Agregar al carrito" type="Large" color="Ghost" />
      </div>

      <div className={classes.Benefits}>
        <div className={classes.Benefit}>
          {props.product.warranty && (
            <>
              <Warranty />
              <p>{props.product.warranty}</p>
            </>
          )}
        </div>
      </div>
    </aside>
  );
};

export default ProductContent;
