import React from "react";
import { Link } from "react-router-dom";
import classes from "./Categories.module.scss";

const Categories = props => {
  return (
    <section className={classes.Categories}>
      {props.categories.map(categorie => {
        return (
          <Link
            to={`/products/categorie/${categorie.id}`}
            className={classes.Categorie}
            key={categorie.id}
          >
            {categorie.name}
          </Link>
        );
      })}
    </section>
  );
};

export default Categories;
