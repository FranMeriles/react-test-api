import React from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

import Search from "./Search/Search";

import Heart from "../../assets/svg/Heart"

import Logo from "../../assets/img/logoMeli.png";
import classes from "./Header.module.scss";

const Header = props => {
  const favorites = useSelector(state => state.favorites.products)
  return (
    <header className={classes.Header}>
      <nav>
        <div className="container">
          <div className={classes.Items}>
            <Link to="/">
              <img
                className={classes.BrandLogo}
                src={Logo}
                alt="Mercado Libre Logo"
              />
            </Link>
            <Search />
            <div className={classes.Favorite}>
              <Link to="/favorites">
                <Heart type="border"/>
                { favorites.length > 0 ? <label>{favorites.length > 9 ? '+9' : favorites.length}</label> : null }
              </Link>
            </div>
          </div>
        </div>
      </nav>
    </header>
  );
};

export default Header;
