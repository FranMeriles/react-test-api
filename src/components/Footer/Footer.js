import React from 'react';

import classes from './Footer.module.scss';

const Footer = () => {
  return (
    <section className={classes.Footer}>
      <div className="container">
        <div className={classes.Content}>
          <div className={classes.Info}>
            <nav>
              <a
                target="_blank"
                rel="noopener noreferrer"
                href="http://www.mercadolibre.com/empleos"
              >
                Trabajá con nosotros
              </a>
              <a
                target="_blank"
                rel="noopener noreferrer"
                href="https://www.mercadolibre.com.ar/ayuda/terminos-y-condiciones-de-uso_991"
              >
                Términos y condiciones
              </a>
              <a
                target="_blank"
                rel="noopener noreferrer"
                href="https://www.mercadolibre.com.ar/ayuda/Politicas-de-privacidad_993"
              >
                Políticas de privacidad
              </a>
              <a
                target="_blank"
                rel="noopener noreferrer"
                href="https://www.mercadolibre.com.ar/ayuda"
              >
                Ayuda
              </a>
            </nav>
            <span>Copyright © 1999-2019 MercadoLibre S.R.L</span>
          </div>
          <div className={classes.App}>
            <a
              href="https://www.mercadolibre.com.ar/gz/app"
              target="_blank"
              rel="noopener noreferrer"
            >
              ¡Descargá gratis la app de Mercado Libre!
            </a>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Footer;
