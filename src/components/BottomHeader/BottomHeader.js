import React from "react"
import { withRouter } from 'react-router';

import ChevronArrow from "../../assets/svg/ChevronArrow";

import classes from "./BottomHeader.module.scss"

const BottomHeader = (props) => {
  return <div className={classes.BottomHeader}>
  <button onClick={() => props.history.goBack()}>
    <ChevronArrow /> Volver
  </button>
</div>
}

export default withRouter(BottomHeader)