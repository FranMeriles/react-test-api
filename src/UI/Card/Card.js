import React from 'react';
import { useDispatch, useSelector } from "react-redux";
import * as favoritesActions from "../../store/actions/favorites"

import { Link } from 'react-router-dom';

import classes from './Card.module.scss';

import Heart from '../../assets/svg/Heart';
import Truck from '../../assets/svg/Truck';

import { getPriceRound } from '../../util/getPriceRound';
import formatPrice from '../../util/formatPrice';
import checkIfProductExistInFavorites from "../../util/checkIfProductExistInFavorites"

const Card = ({ product }) => {
  const dispatch = useDispatch();
  const productsFavorites = useSelector(state => state.favorites.products)
  return (
    <article className={classes.Card}>
      <div className={classes.Favorite}>
      {
        checkIfProductExistInFavorites(productsFavorites, product.id)         
        ?     <div onClick={() => dispatch(favoritesActions.removeFavorite(product.id))}><Heart type="full"/></div>
        :     <div onClick={() => dispatch(favoritesActions.addFavorite(product))}><Heart type="border"/></div>
      } 
      </div>
      <Link to={`/items/${product.id}`}>
        <div className={classes.Image}>
          <img src={product.thumbnail} alt={product.title} />            
        </div>
        <div className={classes.Content}>
          {product.shipping.free_shipping && <Truck />}

          <div className={classes.Price}>
            <p>{formatPrice(product.price)}</p>
            {product.original_price ? (
              <span>
                {getPriceRound(product.original_price, product.price)}
                %OFF
              </span>
            ) : null}
          </div>

          <span className={classes.Title}>{product.title}</span>
        </div>
      </Link>
    </article>
  );
};

export default Card;
