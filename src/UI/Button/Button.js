import React from 'react';
import classes from './Button.module.scss';

const Button = props => {
  return (
    <button
      className={`${classes.Button} ${classes[props.type]} ${
        classes[props.color]
      }`}
    >
      {props.text}
    </button>
  );
};

export default Button;
