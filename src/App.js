import React, { useEffect } from 'react';
import './App.scss';
import Layout from './hoc/Layout/Layout';
import { Route } from 'react-router-dom';

import Home from './containers/Home/Home';
import ProductsByCategory from './containers/ProductsByCategory/ProductsByCategory';
import ProductsBySearch from './containers/ProductsBySearch/ProductsBySearch';
import Favorites from "./containers/Favorites/Favorites"

import Product from './containers/Product/Product';
import * as favoritesActions from "./store/actions/favorites";
import { useDispatch } from "react-redux"


const App = props => {
  const dispatch = useDispatch()

  useEffect(() => {
    const favoriteProductsLocalStorage = localStorage.getItem("favoriteProducts");
    if (favoriteProductsLocalStorage) {
      dispatch(favoritesActions.insertAllFavorites(JSON.parse(favoriteProductsLocalStorage)))
    }

  }, [dispatch])

  return (
      <Layout>
        <div className="container">
          <Route path="/" exact component={Home} />
          <Route path="/products/categorie/:id" component={ProductsByCategory} />
          <Route path="/items/:id" component={Product} />
          <Route path="/search" component={ProductsBySearch} />
          <Route path="/favorites" component={Favorites} />
        </div>
      </Layout>
  );
};

export default App;
