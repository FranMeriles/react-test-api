export const getPriceRound = (highPrice, lowPrice) => {
  return Math.round(((highPrice - lowPrice) / highPrice) * 100);
};
