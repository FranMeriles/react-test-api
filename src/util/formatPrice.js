import currency from "currency.js"

const peso = value => currency(value, { separator: ".", decimal: ",", symbol: "$ ", formatWithSymbol: true })

const formatPrice = price => {
  if (price) {
    // THIS NOT WORK IN ALL BROWSERS
    // return price.toLocaleString("es-AR", {
    //   style: "currency",
    //   currency: "ARS"
    // });
    return peso(price).format()
  }
};

export default formatPrice;
