const QUANTITY_STARS = 5;

const getStarsToRate = (rate) => {
  const rates = [];
  for (let i = 1; i <= QUANTITY_STARS; i++) {
    const star = rate >= i ? 'full' : rate > i-1 ? 'half' : 'full';
    rates.push(star);
  }
  return rates
}

export default getStarsToRate
