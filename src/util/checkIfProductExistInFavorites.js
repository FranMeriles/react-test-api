export default (products, productId) => products.some(product => product.id === productId)
