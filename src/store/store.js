import { createStore, combineReducers } from "redux";

import favoriteReducer from "./reducers/favorite";

const rootReducer = combineReducers({
  favorites: favoriteReducer
});

const store = createStore(
  rootReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__() 
);

export default store;