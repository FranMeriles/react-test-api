import React from "react";

const Warranty = () => {
  return (
    <svg
      viewBox="0 0 16 16"
      id="ui-icon--warranty-badge"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g fill="none">
        <path
          d="M14.693 3C12.98 3 10.24 1.594 8.423.2a.96.96 0 0 0-1.174 0C5.43 1.594 2.693 3 .98 3 .44 3 0 3.45 0 4c0 5.585 2.646 9.818 7.45 11.92a.964.964 0 0 0 .772 0c4.805-2.102 7.45-6.335 7.45-11.92a.99.99 0 0 0-.98-1z"
          fill="#BBB"
        ></path>
        <path
          fill="#FFF"
          d="M12.244 6.2l-1.37-1.4-3.92 4-1.958-2-1.372 1.4 3.33 3.4"
        ></path>
      </g>
    </svg>
  );
};

export default Warranty;
